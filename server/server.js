const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const chalk = require('chalk');

const prettyPrint = require('./utils').prettyPrint;

const baseUrl = process.env.npm_package_config_baseUrl;

const provincias = [
    {'codigo':1, 'descripcion':'Asturias'},
    {'codigo':2, 'descripcion':'Alicante'},
    {'codigo':3, 'descripcion':'Cartagena'},
    {'codigo':4, 'descripcion':'Madrid'},
    {'codigo':5, 'descripcion':'Barcelona'},
    {'codigo':6, 'descripcion':'Huelva'},
    {'codigo':7, 'descripcion':'Jaén'} ]

const tiposVia = [
    {'codigo': 'AV', 'descripcion': 'Avenida'},
    {'codigo': 'CL', 'descripcion': 'Calle'},
    {'codigo': 'PZ', 'descripcion': 'Plaza'},
    {'codigo': 'XX', 'descripcion': 'where'}
]

const poblaciones = [
    {'codigoProvincia': '08',
    'provincia':'BARCELONA',
    'codigoMunicipio':'019',
    'municipio':'Barcelona',
    'codigoPoblacion':'08019000101',
    'poblacion':'Barcelona',
    'indicadorCapitalidad': 'MunicipioYPostal'},
    {'codigoProvincia': '09',
    'provincia':'MADRID',
    'codigoMunicipio':'020',
    'municipio':'Madrid',
    'codigoPoblacion':'0801912343245',
    'poblacion':'Madrid',
    'indicadorCapitalidad': 'MunicipioYPostal'},
    {'codigoProvincia': '10',
    'provincia':'JAEN',
    'codigoMunicipio':'023',
    'municipio':'Jaen',
    'codigoPoblacion':'0801916542346',
    'poblacion':'Jaen',
    'indicadorCapitalidad': 'MunicipioYPostal'}]

const vias = [
        {'codigoTipoVia': 'Av',
        'codigoVia':'6604',
        'via':'Madrid',
        'codigoPostal':'28006'},
        {'codigoTipoVia': 'Cl',
        'codigoVia':'MADRID',
        'via':'Bravo Murillo',
        'codigoPostal':'28020'},
        {'codigoTipoVia': 'Pz',
        'codigoVia':'JAEN',
        'via':'Chamberi',
        'codigoPostal':'28003'}
        ]

const domicilio = {
      "codigoProvincia": "08",
      "provincia": "BARCELONA",
      "codigoPoblacion": "08019000101",
      "poblacion": "BARCELONA",
      "poblacionNormalizada": true,
      "codigoTipoVia": "Av",
      "codigoVia": "6604",
      "via": "Toreros",
      "viaNormalizada": "Normalizado"
  };

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    // res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

//support parsing of application/json type post data
app.use(bodyParser.json());


//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));


//tell express what to do when the /about route is requested


//post example
// app.post('/solicitudes',function(req, res){
// 	res.setHeader('Content-Type', 'application/json');
//  	// console.log(req.header.Authorization);
// 	//mimic a slow network connection
// 	setTimeout(function(){
// 		res.send(JSON.stringify(solicitud));
// 	}, 1000)
//
// 	//debugging output for the terminal
// 	const date = new Date();
// 	console.log(chalk.bold.green(`${req.path}: ${req.method} (${date})`));
// });
app.get('/provincias',function(req, res){
	res.setHeader('Content-Type', 'application/json');
 	// console.log(req.header.Authorization);
	//mimic a slow network connection
	setTimeout(function(){
		res.send(JSON.stringify(provincias));
	}, 1000)

	//debugging output for the terminal
	const date = new Date();
	console.log(chalk.bold.green(`${req.path}: ${req.method} (${date})`));
});
app.get('/tiposVia',function(req, res){
	res.setHeader('Content-Type', 'application/json');
 	// console.log(req.header.Authorization);
	//mimic a slow network connection
	setTimeout(function(){
		res.send(JSON.stringify(tiposVia));
	}, 1000)

	//debugging output for the terminal
	const date = new Date();
	console.log(chalk.bold.green(`${req.path}: ${req.method} (${date})`));
});
app.get('/poblaciones',function(req, res){
	res.setHeader('Content-Type', 'application/json');
 	// console.log(req.header.Authorization);
	//mimic a slow network connection
	setTimeout(function(){
		res.send(JSON.stringify(poblaciones));
	}, 1000)

	//debugging output for the terminal
	const date = new Date();
	console.log(chalk.bold.green(`${req.path}: ${req.method} (${date})`));
});
app.get('/vias',function(req, res){
	res.setHeader('Content-Type', 'application/json');
 	// console.log(req.header.Authorization);
	//mimic a slow network connection
	setTimeout(function(){
		res.send(JSON.stringify(vias));
	}, 1000)

	//debugging output for the terminal
	const date = new Date();
	console.log(chalk.bold.green(`${req.path}: ${req.method} (${date})`));
});
app.get('/domicilios',function(req, res){
	res.setHeader('Content-Type', 'application/json');
 	// console.log(req.header.Authorization);
	//mimic a slow network connection
	setTimeout(function(){
		res.send(JSON.stringify(domicilio));
	}, 1000)

	//debugging output for the terminal
	const date = new Date();
	console.log(chalk.bold.green(`${req.path}: ${req.method} (${date})`));
});

//wait for a connection
const server = app.listen(3000, () => {
    console.log(chalk.blue.bold(`Server listening on localhost: ${server.address().port}`));
});
