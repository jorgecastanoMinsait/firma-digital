const cJson = require('circular-json');

const prettyPrint = (tittle = '', text, identation = 2) => {
  if (tittle !== '') {
    return `***${tittle}***\r\n${cJson.stringify(text, undefined, identation)}\r\n`;
  }
  return cJson.stringify(text, undefined, identation);
};

module.exports = {
    prettyPrint
};
