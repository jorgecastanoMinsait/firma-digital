import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '@app/shared/shared.module';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: '@app/modules/home/home.module#HomeModule'
  },
  {
    path: 'users',
    loadChildren: '@app/modules/users/users.module#UsersModule'
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes), SharedModule ],
  exports: [ RouterModule ]
})
export class AppRoutingModule  { }
