import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { CoreModule } from '@app/core/core.module';
import { SharedModule } from '@app/shared/shared.module';

// import { HomeModule } from '@app/modules/home/home.module';
// import { UsersModule } from '@app/modules/users/users.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FirmaDigitalComponent } from '@app/modules/firma-digital/firma-digital.component';



@NgModule({
  declarations: [
    AppComponent,
    FirmaDigitalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    CoreModule,
    SharedModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
