import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from '@app/core/providers/user/user.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [ UserService ],
  exports: [

  ]
})
export class CoreModule {
    /* make sure CoreModule is imported only by one NgModule the AppModule */
      constructor (
        @Optional() @SkipSelf() parentModule: CoreModule
      ) {
        if (parentModule) {
          throw new Error('CoreModule is already loaded. Import only in AppModule');
        }
      }
}
