import { TestBed, inject, async } from '@angular/core/testing';
import {HttpClient} from '@angular/common/http';
// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { User } from '@app/core/models/user';
import { UserService } from './user.service';

describe('UserService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule],
          providers: [
            UserService,
          ]
        });

        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);

    });

     afterEach(() => {
       // After every test, assert that there are no more pending requests.
       httpTestingController.verify();
     });
    it('should be created', inject([UserService], (service: UserService) => {
        expect(service).toBeTruthy();
    }));


});
