import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { catchError, map, tap } from 'rxjs/operators';

import { User } from '@app/core/models/user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

    private usersUrl = 'api/users';  // URL to web api

    constructor(private http: HttpClient) { }

    getUsers(): Observable<User[]>  {
        return this.http.get<User[]>(this.usersUrl);
    }

    getUser(id: number): Observable<User>  {
        const url = `${this.usersUrl}/${id}`;
        return this.http.get<User>(url);
    }

    addUser(user: User): Observable <any> {
        return this.http.post<User>(this.usersUrl, user, httpOptions);
    }

    updateUser(user: User): Observable<any> {
        return this.http.put(this.usersUrl, user, httpOptions);
    }

    delete(id: number): Observable <User> {
        const url = `${this.usersUrl}/${id}`;
        return this.http.delete<User>(url, httpOptions);
    }
}
