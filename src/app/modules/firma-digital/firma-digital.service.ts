import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { XmlData } from './models/xmlData';

// import { environment } from '@env/environment';


@Injectable()
export class FirmaDigitalService {

  url = '/firmaDigital';
  redirectUrl = '/redirectUrl';

  constructor(private http: HttpClient) { }

  getXmlData(): Observable<{} | XmlData> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    console.log('getXmlData');

    return this.http.post<XmlData>(this.url, {}, httpOptions).pipe(
      tap((solicitud: XmlData) => console.log(`getXmlData`)),
      catchError(this.handleError('getXmlData'))
    );
  }

  redirect(): Observable<{} | XmlData> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/xml' })
      // headers: new HttpHeaders({ 'Content-Type': 'text/xml' })
    };
    console.log('redirect');

    return this.http.post<XmlData>(this.redirectUrl, {}, httpOptions).pipe(
      tap((solicitud: XmlData) => console.log(`redirect`)),
      catchError(this.handleError('redirect'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<HttpErrorCode> (operation = 'operation', result?: HttpErrorCode) {
    return (error: any): Observable<HttpErrorCode> => {

      console.error('Error ' + error.status + ' ' + error.statusText); // log to console
      // Let the app keep running by returning an empty result.
      return of(error as HttpErrorCode);
    };
  }
}
