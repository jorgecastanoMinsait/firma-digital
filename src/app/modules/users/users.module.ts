import { NgModule } from '@angular/core';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users-list/users.component';

@NgModule({
  imports: [
    UsersRoutingModule
  ],
  declarations: [ UsersComponent ]
})
export class UsersModule { }
